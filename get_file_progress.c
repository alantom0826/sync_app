/*  GFP ==> get file progress  */

#include <string.h>
#include <math.h>
#include <glib.h>
#include <gtk/gtk.h>

GtkWidget *GFP_window;
GtkWidget *GFP_file_lable, *GFP_msg_lable;
GtkWidget *GFP_ok_button;
GTimeVal startime, pausedtime;
gint global_i = 1;
gboolean All_Done = FALSE;
gdouble done = 0;
extern GtkBuilder  *main_builder;
extern gint finished;
extern rdownload_file_teacher();
extern int dir_total;
extern gint check_shomework;

/// progress of completed ///
void completed_progress(GString *str)
{
    gchar tmps[50];
    glong elapsed, remaining;
    GTimeVal curtime;
    GTimeVal tmptime;
    
    GtkProgressBar *progressGBar = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_gbar"));
    GtkProgressBar *progressSBar = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_sbar"));
    GtkLabel *tmpl = (GtkLabel*) gtk_builder_get_object(main_builder, "progress_file_lab");
    g_get_current_time(&curtime);
    elapsed = curtime.tv_sec - startime.tv_sec;
    remaining = elapsed / 0.1 - elapsed;
    
    //gtk_label_set_text(tmpl, "成功完成!!\n");
    gtk_label_set_markup(tmpl, "<span foreground='darkgreen' weight='bold'>Completed successfully!</span>\n");
    All_Done = TRUE;
    done = 1.0;
    snprintf(tmps, 49, "%.f%% (%li:%02li elapsed, 00:00 remaining)", round(done * 100), elapsed / 60, elapsed % 60, remaining / 60, remaining % 60);
    gtk_progress_bar_set_fraction(progressGBar, done);
    gtk_progress_bar_set_text(progressGBar, tmps);
        
    snprintf(tmps, 5, "%.f%%",round(done * 100));
    gtk_progress_bar_set_fraction(progressSBar, done);
    gtk_progress_bar_set_text(progressSBar, tmps);
}

/// quit progressbar window ///
void GFP_window_quit()
{
    gchar tmps[50];
    
    GtkLabel *tmpl = (GtkLabel*) gtk_builder_get_object(main_builder, "progress_file_lab");
    GtkProgressBar *progressSBar = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_sbar"));
    
    //if (!strcasecmp(gtk_label_get_text(tmpl), "成功完成!!\n")){
    if (All_Done == TRUE){
    gtk_widget_hide(GFP_window);
    global_i = 1;
    
    strcpy(tmps, "0%");
    gtk_progress_bar_set_fraction(progressSBar, 0);
    gtk_progress_bar_set_text(progressSBar, tmps);
    gtk_label_set_text(tmpl, "Waiting....\n");
    dir_total = 0;
    }
}


/// signal file progress///
gboolean set_file_progress(gdouble fraction) 
{
    GTimeVal curtime;
    static glong lastupdate = 0;
    gchar tmps[50];
    GString *text;

    GtkProgressBar *progressSBar = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_sbar"));
    
    g_get_current_time(&curtime);
    if (fraction < (gdouble)1 && curtime.tv_sec == lastupdate) return;
    lastupdate = curtime.tv_sec;
    
    gtk_progress_bar_set_fraction(progressSBar, fraction);
    snprintf(tmps, 5, "%.f%%",round(fraction * 100));
    gtk_progress_bar_set_text(progressSBar, tmps);
    
    return TRUE;
}

/// global file progress ///
gboolean set_global_progress(gdouble fraction, GString *str)
{
    glong elapsed, remaining;
    static glong lastupdate = 0;
    gchar tmps[50];
    GTimeVal curtime;
    GTimeVal tmptime;
    
    GtkProgressBar *progressGBar = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_gbar"));
    gtk_progress_bar_set_fraction(progressGBar, 0);
    g_get_current_time(&curtime);
    //g_get_current_time(&tmptime);
    //g_get_current_time(&pausedtime);
    if (fraction > (gdouble)0 && fraction < (gdouble)1 && curtime.tv_sec == lastupdate) return;
    lastupdate = curtime.tv_sec;
    
    gtk_progress_bar_set_fraction(progressGBar, fraction);
    //startime.tv_sec += tmptime.tv_sec - pausedtime.tv_sec;
    elapsed = curtime.tv_sec - startime.tv_sec;
    remaining = elapsed / fraction - elapsed;
    
    if (dir_total == 0) dir_total = 1;   
    if (fraction) snprintf(tmps, 49, "%.f%% (%li:%02li elapsed, %li:%02li remaining)", (round(fraction * 100)/dir_total)*global_i, elapsed / 60, elapsed % 60, remaining / 60, remaining % 60);
    else strcpy(tmps, "0%"); 
    
    gtk_progress_bar_set_text(progressGBar, tmps);
    
    return TRUE;
}

/// IO function ///
gboolean iochannel_read(GIOChannel *channel, GIOCondition condition, gpointer progr) 
{
    gdouble fraction, total;
    GString *str;
    gchar *file;
    
    GtkLabel *tmpl = (GtkLabel*) gtk_builder_get_object(main_builder, "progress_file_lab");
    
    str = g_string_new("");
    g_io_channel_read_line_string(channel, str, NULL, NULL);

    if (strchr(str->str, '%') && str->str[0] == ' ') {
        printf("======%s=====\n", str->str);
        fraction = g_ascii_strtod(strchr(strchr(str->str, '%') - 4, ' '), NULL) / 100;
        set_file_progress(fraction);
        
        /// 計算全部進度
        /*fraction = 0;
        if (strchr(str->str, '%') != strrchr(str->str, '%')) fraction = g_ascii_strtod(strchr(strrchr(str->str, '%') - 6, ' '), NULL) / 100;
        else if (strchr(str->str, '=')) {
            total = g_ascii_strtod(strrchr(str->str, '/') + 1, NULL);
            done = total - g_ascii_strtod(strrchr(str->str, '=') + 1, NULL);
            fraction = done / total;
        }
        if (fraction) set_global_progress(fraction, str);*/
    }
    
    file = g_strconcat(str->str, NULL);
    g_strchomp(str->str);
    
    if (str->str[0] != 0 && !strchr(str->str, '%') && !strstr(str->str, "bytes") && !strstr(str->str, "receiving")){        
        if (strstr(str->str, "speedup")){
            g_strlcpy(file, "Waiting....\n",20);
            printf("====finished====%d\n", finished);
            printf("====dir_total====%d\n", dir_total);

            if (finished == dir_total-1 || (finished == 0 && dir_total ==0)){ 
                g_strlcpy(file, "Completed successfully!\n",50);
                completed_progress(str);
            }
            if (finished <= dir_total-1){
                finished++;
                global_i++; 
                rdownload_file_teacher();
            }
            if (check_shomework == 1) check_s_homework();
        }
        if (file[0] != 0){
            if(!strcasecmp(file, "Completed successfully!\n")){
                gtk_label_set_markup(tmpl, "<span foreground='darkgreen' font_weight='bold'>Completed successfully!</span>\n");
            }else{
                gtk_label_set_text(tmpl, file);
            }
        }
        done = 0;
        All_Done = TRUE;
        g_free(file);
    }
    
    return TRUE;
}


/// ProgressBar main function ///
int get_file_progress (gchar **cmd) {

    gint *output;
    gchar *buffer;
    gchar buffer1[1024];
    GString *str;
    GIOChannel *chout, *cherr;
    gsize length;
    GtkTextView *view;
    gint timer;


    GFP_window = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_win"));
    GFP_msg_lable = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_msg_lab"));
    view = GTK_WIDGET(gtk_builder_get_object(main_builder, "textview_output"));
    GFP_ok_button = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_ok_btn"));
    
    g_spawn_async_with_pipes(NULL, cmd, NULL, 0, NULL, NULL, NULL, NULL, &output, NULL, NULL);
    //g_spawn_sync(NULL, cmd, NULL, 0, NULL, NULL, &output, NULL, NULL, NULL);
    
    g_print("output --> %d\n", output);
    
    chout = g_io_channel_unix_new(output);
    g_io_channel_set_flags(chout, G_IO_FLAG_NONBLOCK, NULL);
 
    if(!g_io_add_watch_full(chout, G_PRIORITY_DEFAULT_IDLE - 10, G_IO_IN , (GIOFunc) iochannel_read, (gpointer) view, NULL)){
        g_error("錯誤: 無法對 GIOChannel 加入 watch！\n");
    }
    /// g_timeout_add(20, abc, (gpointer)output);
    
    g_io_channel_set_close_on_unref(chout, TRUE);
    g_signal_connect(GTK_OBJECT(GFP_window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(GTK_OBJECT(GFP_ok_button), "clicked", G_CALLBACK(GFP_window_quit), NULL);

    gtk_widget_show(GFP_window);
    g_get_current_time(&startime);
    
    return 0;
}



/// test area ///
/*static gboolean abc(gpointer user_data){
    char buffer[255];
    int output = (int)user_data;
    gfloat fraction;
    
    read(output, buffer, 255);
    g_print("buffer=%s\n", buffer);
    if (buffer!='\0')
    if (strchr(buffer, '%') && *buffer == ' ') {
        fraction = g_ascii_strtod(strchr(strchr(buffer, '%') - 4, ' '), NULL) / 100;
        printf("[www%s]======%f======\n",__func__, fraction);
    }

    //close(output);
    set_file_progress(fraction);
#if 0
    //for(; f<=1.0; f+=0.01)
    {
        g_print(">>>%f\n", f);
        set_file_progress(f);
        f+=0.1;
    }
    return 1;
#endif
}*/
