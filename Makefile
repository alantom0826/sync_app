appname	:=	mysync
pappname := mysync_p
pkgname	:= sync_app
pngname	:=	sync_tray
sshname:= sshkey
uiname	:= main
version	:=	0.1
usrdir	:=	/usr/share
PKGCONFIG	:=	pkg-config
LIB	:=	--cflags --libs
libcsync	:=	-lcsync
libmath := -lm
SRC	:=	main.c rsyncfile.c get_json_file.c get_homefile.c upload_file.c get_file_progress.c
ARG := -w
GTK	:=	gtk+-2.0
GMOD := gmodule-2.0
CC	:=	gcc
datadir	:= usr/share
bindir	:= usr/bin
pixmapsdir	:= usr/share/pixmaps
sshdir := root/.ssh

.PHONY:all clean install

all:
	$(CC) $(ARG) $(SRC) -o $(pkgname) `$(PKGCONFIG) $(LIB) $(GTK) $(GMOD)` $(libmath)
w:
	$(CC) $(SRC) -o $(pkgname) `$(PKGCONFIG) $(LIB) $(GTK) $(GMOD)` $(libmath)
install:
	mkdir -p $(RPM_BUILD_ROOT)/$(datadir)/$(pkgname)
	install -m755 $(pkgname) $(RPM_BUILD_ROOT)/$(datadir)/$(pkgname)/
	install -m755 $(uiname).glade $(RPM_BUILD_ROOT)/$(datadir)/$(pkgname)/$(uiname).glade
	mkdir -p $(RPM_BUILD_ROOT)/$(bindir)/
	install -m755 $(appname) $(RPM_BUILD_ROOT)/$(bindir)/
	install -m755 $(pappname) $(RPM_BUILD_ROOT)/$(bindir)/
	mkdir -p $(RPM_BUILD_ROOT)/$(sshdir)/
	install -m755 $(sshname) $(RPM_BUILD_ROOT)/$(sshdir)/
	mkdir -p $(RPM_BUILD_ROOT)/$(pixmapsdir)
	install -m755 image/*.png $(RPM_BUILD_ROOT)/$(pixmapsdir)/
	mkdir -p  $(RPM_BUILD_ROOT)/$(datadir)/applications/
	install -Dm 644 $(pkgname).desktop $(RPM_BUILD_ROOT)/$(datadir)/applications/$(pkgname).desktop
