#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define command_size 200

struct argument_s {
  char *args[2]; /* SOURCE and DESTINATION */
  char *exclude_file;
  int disable_statedb;
  int create_statedb;
  int update;
  int reconcile;
  int propagate;
  int check_upload;
  int check_dowload;
  int check_filelist_download;
};
struct argument_s arguments;

gchar rupload_use[command_size];
gchar rdownload_use[command_size];
gchar rename_use[command_size];

int rdownload_file();
void rupload_file();
int create_catalog_dir(char *pathname);
