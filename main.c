/*  pathname ==> path/filename  */

#include <argp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#include <gtk/gtk.h>
#include "csync.h"
//#include "csyncfile.h"
#include "rsyncfile.h"
#include "common.h"
#include "get_json_file.h"

GtkBuilder  *main_builder;
GtkWidget   *setup_window ,*UFW_window;
GtkWidget   *padmode_window;  
GtkWidget   *remote_edit, *local_edit;

const gchar *remote_target_add, *local_target_add, *remote_upload_add;
gchar remote_target_total[100];
gchar remote_target_student_total[100];
extern const gchar *get_json_file(char *);
extern int download_backup_hw();
extern int download_backup_hw1();
extern int get_homefile();
extern void upload_file();
extern void check_s_homework();

// 之後由 json 檔去抓取
//~ const gchar *account_name = "alan";
//~ const gchar *account_passwd = "123456";
//~ const gchar *user_name = "林大中";
//~ const gchar *user_sn = "S001";
//~ const gchar *user_type;
//~ const gchar *account_ftp;
const gchar *com_account_name = "alan";
const gchar *com_account_passwd = "123456";

int setup_dpy = 1;
void setup_switch_win();

static void usrdir_toggle()
{
    if (!checkboxs.usrdir_f){
        printf("usrdir_f_open\n");
        checkboxs.usrdir_f = 1;
    }else{
        printf("usrdir_f_close\n");
        checkboxs.usrdir_f = 0;
    }
}

gint delete_event( GtkWidget *widget, GdkEvent  *event, gpointer   data )
{
/* 如果你的 "delete_event" 信號處理函式傳回 FALSE，GTK 會發出 "destroy" 信號。
 * 傳回 TRUE，你不希望關閉視窗。
 * 當你想彈出「你確定要退出嗎?」對話框時它很有用。*/
    g_print ("delete event occurred\n");
    setup_switch_win();
/* 改 TRUE 為 FALSE 程式會關閉。*/
    return TRUE;
}

int rename_func( const char *pathname, const struct stat *statptr, int type)
{
    if (type == 0){
        
        if(!strstr(pathname, user_name))
        {
            printf("%s\n", basename(pathname));
            char file_old_name[200], file_new_name[200];
            sprintf(file_old_name, "/root/homework/%s", basename(pathname));
            // 有加sn
            //sprintf(file_new_name, "/root/home/%s_%s_%s", user_name, user_sn, basename(pathname));
            // 沒加sn            
            sprintf(file_new_name, "/root/homework/%s_%s", user_name, basename(pathname));            
            rename(file_old_name, file_new_name);
        }
    }
        return 0;
}

int rename_file()
{
    char path[100];
    int  ret;

    strcpy(path, local_source);
    ret = ftw(path, rename_func, 3);
    if(ret == -1){
        printf("ftw error\n");
        return -1;
    }else{
        printf("操作成功\n");
        return 0;
    }
}

static void target_combine_csync()
{
    g_snprintf(remote_target_total,100,"%s://%s:%s@%s:%s", 
                                        sftp_cmd, account_name, 
                                        account_passwd, 
                                        remote_target_add, remote_dest);
}

static void target_combine_rsync()
{
    memset( rdownload_use, 0, strlen(rdownload_use) ); 
    memset( rupload_use, 0, strlen(rupload_use) );
    memset( rename_use, 0, strlen(rename_use) );     
    
    g_snprintf(remote_target_total,100,"%s@%s:%s", 
                                        com_account_name, 
                                        remote_target_add, remote_dest);
    g_snprintf(remote_target_student_total,100,"%s@%s:%s%s", 
                                        com_account_name, 
                                        remote_target_add, remote_student_dest, user_name);                                            
    g_snprintf(rdownload_use,command_size,"%s -p %s %s -av %s %s %s %s",
                                        sshpass_cmd, com_account_passwd, 
                                        rsync_cmd, ingnore_exist,
                                        remote_target_total, local_target_add, check_finish );
    g_snprintf(rupload_use,command_size,"%s -p %s %s -av %s %s %s",
                                        sshpass_cmd, com_account_passwd, 
                                        rsync_cmd,local_target_add,
                                        remote_target_student_total, check_finish);
    // use rename_cmd
    //g_snprintf(rename_use,command_size,"%s %s",rename_cmd, prefix_add);
    if (!strcasecmp(user_type, "student")){ 
        g_printf("show rdownload_use ==> %s\n", rdownload_use);
        g_printf("show rupload_use   ==> %s\n", rupload_use);
    }
}

static void init_all()
{ 
    strncpy(user_name, get_json_file(NAME), 50);
    strncpy(user_type, get_json_file(TYPE), 50);
    //~ strncpy(user_sn, get_json_file(SN), 50);    
    //~ strncpy(account_ftp, get_json_file(FTP), 50);    
    //~ strncpy(account_name, get_json_file(ACCOUNT), 50);
    //~ strncpy(account_passwd, get_json_file(PASSWD), 50); 
}

static void setup_ok_clicked()
{
    remote_target_add = gtk_entry_get_text(GTK_ENTRY(remote_edit));
    local_target_add = gtk_entry_get_text(GTK_ENTRY(local_edit));

    //target_combine_csync();
    target_combine_rsync();
    setup_switch_win();
}

void setup_init()
{
    remote_target_add = remote_entry_default;
    local_target_add = local_source;
    target_combine_rsync();
}

void sync_upload_result() 
{
    GtkWidget *dialog;
    if (arguments.check_upload){
        MSG_SEND = MSG_UOK;
        dialog = gtk_message_dialog_new(NULL, 
                    GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, 
                    GTK_BUTTONS_OK, MSG_SEND);
        
    }else{
        MSG_SEND = MSG_UNG;
        dialog = gtk_message_dialog_new(NULL, 
                    GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, 
                    GTK_BUTTONS_OK, MSG_SEND);
    }
    
    sleep(2);
    gtk_window_set_title(GTK_WINDOW(dialog), "提示訊息");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

void sync_download_result() 
{        
    GtkWidget *dialog;
    if (arguments.check_dowload){
        MSG_SEND = MSG_DOK;
        dialog = gtk_message_dialog_new(NULL, 
                    GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, 
                    GTK_BUTTONS_OK, MSG_SEND);
    }else{
        MSG_SEND = MSG_DNG;
        dialog = gtk_message_dialog_new(NULL, 
                    GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, 
                    GTK_BUTTONS_OK, MSG_SEND);
    }
    
    sleep(2);
    gtk_window_set_title(GTK_WINDOW(dialog), "提示訊息");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

static void rupload_data()
{
    rupload_file();
    //sync_upload_result();
}

static void rdownload_data()
{
    //csync_file(local_source, remote_target_total);
    download_backup_hw1();
    rdownload_file();
    //sync_download_result();
    rename_file();
}

// 滑鼠右鍵event tray選單
//~ static void trayIconPopup(GtkStatusIcon *status_icon, 
                            //~ guint button, guint32 activate_time, 
                            //~ gpointer popUpMenu)
//~ {
    //~ gtk_menu_popup(GTK_MENU(popUpMenu), NULL, NULL, 
                            //~ gtk_status_icon_position_menu, 
                            //~ status_icon, button, activate_time);
//~ }

static void trayIconPopup(GObject *trayIcon, GtkWidget * menu)
{
    gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
                    gtk_status_icon_position_menu, trayIcon,
                    1, gtk_get_current_event_time ());
}

static int window_close(GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
    return 0;
}

void setup_switch_win()
{	  
	if (setup_dpy == 1){
		gtk_widget_show(setup_window);		
		setup_dpy = 0;
	}else{
		gtk_widget_hide(setup_window);
		setup_dpy = 1;
	}        
}

static int setup_backup_hw()
{
    if(!strcasecmp(user_type, "student")){
        download_backup_hw();
    }
    return 0;
}

void p_upload()
{   
    if (!strcasecmp(user_type, "teacher")){
        upload_file();
    }else{
        rupload_data();
    }
}

void p_download()
{
    if (!strcasecmp(user_type, "teacher")){
        get_homefile();
    }else{
        rdownload_data();
    }
}


int main(int argc,char *argv[])
{
	init_all();
    setup_init();

    GtkStatusIcon *tray_icon;
    GtkWidget *menu, *menuItemDownload_stu, *menuItemUpload_stu, *menuItemSetup, *menuSeparator; 
    GtkWidget *menuItemDownload_teac, *menuItemUpload_teac;    
    GtkWidget *setup_cancel_btn, *setup_ok_btn;
    GtkWidget *setup_backup_hw_btn;
    GtkWidget *checkbox1, *checkbox2, *checkbox3, *checkbox4, *checkbox5, *checkbox6;
    GtkWidget *menu_download_img, *menu_upload_img, *menu_setup_img;
    GtkWidget *menu_download_img2, *menu_upload_img2;
    GtkWidget *padmode_download_img, *padmode_upload_img, *padmode_setup_img;
    GtkWidget *padmode_upload_btn, *padmode_download_btn;
 	
    gtk_init(&argc,&argv);
	main_builder = gtk_builder_new();    
    setup_ok_btn = gtk_button_new();    
    setup_cancel_btn = gtk_button_new();
    setup_backup_hw_btn = gtk_button_new();
    checkbox1 = gtk_check_button_new();
    checkbox2 = gtk_check_button_new();
    checkbox3 = gtk_check_button_new();
    checkbox4 = gtk_check_button_new();
    checkbox5 = gtk_check_button_new();
    checkbox6 = gtk_check_button_new();
	tray_icon = gtk_status_icon_new();    
    menu = gtk_menu_new();
    menuSeparator = gtk_separator_menu_item_new();
    remote_edit = gtk_entry_new();
    local_edit = gtk_entry_new();
    menu_download_img = gtk_image_new_from_file("/usr/share/pixmaps/menu_download.png");
    menu_upload_img = gtk_image_new_from_file("/usr/share/pixmaps/menu_upload.png");
    menu_setup_img = gtk_image_new_from_file("/usr/share/pixmaps/menu_setup.png");
    menu_download_img2 = gtk_image_new_from_file("/usr/share/pixmaps/menu_download.png");
    menu_upload_img2 = gtk_image_new_from_file("/usr/share/pixmaps/menu_upload.png");
 
    
    menuItemUpload_stu = gtk_image_menu_item_new_with_mnemonic ("上傳作業");
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuItemUpload_stu), menu_upload_img);
	gtk_image_menu_item_set_always_show_image (GTK_IMAGE_MENU_ITEM (menuItemUpload_stu), TRUE);
    menuItemDownload_stu = gtk_image_menu_item_new_with_mnemonic ("下載作業");
	gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuItemDownload_stu), menu_download_img);
	gtk_image_menu_item_set_always_show_image (GTK_IMAGE_MENU_ITEM (menuItemDownload_stu), TRUE);
    menuItemUpload_teac = gtk_image_menu_item_new_with_mnemonic ("上傳作業_teacher");
    gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuItemUpload_teac), menu_upload_img2);
 	gtk_image_menu_item_set_always_show_image (GTK_IMAGE_MENU_ITEM (menuItemUpload_teac), TRUE);
    menuItemDownload_teac = gtk_image_menu_item_new_with_mnemonic ("下載作業_teacher");
    gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM (menuItemDownload_teac), menu_download_img2);
 	gtk_image_menu_item_set_always_show_image (GTK_IMAGE_MENU_ITEM (menuItemDownload_teac), TRUE);    
    menuItemSetup = gtk_image_menu_item_new_with_mnemonic ("設定");
    gtk_image_menu_item_set_image (GTK_IMAGE_MENU_ITEM(menuItemSetup), menu_setup_img);
    gtk_image_menu_item_set_always_show_image (GTK_IMAGE_MENU_ITEM (menuItemSetup), TRUE);
	gtk_builder_add_from_file(main_builder,"main.glade",NULL);    
    gtk_status_icon_set_from_file(tray_icon, sync_png);
    gtk_status_icon_set_tooltip(tray_icon, "同步軟體");


    setup_window = GTK_WIDGET(gtk_builder_get_object(main_builder,"setup_win"));
    setup_ok_btn = GTK_WIDGET(gtk_builder_get_object(main_builder,"setup_ok_btn"));
    setup_cancel_btn = GTK_WIDGET(gtk_builder_get_object(main_builder,"setup_cancel_btn"));
    setup_backup_hw_btn = GTK_WIDGET(gtk_builder_get_object(main_builder,"setup_backup_hw_btn"));
    //progress_window = GTK_WIDGET(gtk_builder_get_object(main_builder,"progress_win"));
    UFW_window = GTK_WIDGET(gtk_builder_get_object(main_builder,"UFW_win"));
    checkbox1 = GTK_WIDGET(gtk_builder_get_object(main_builder,"checkbutton1"));
    remote_edit = GTK_WIDGET(gtk_builder_get_object(main_builder,"remote_entry"));
    local_edit = GTK_WIDGET(gtk_builder_get_object(main_builder,"local_entry"));
    gtk_entry_set_text(GTK_ENTRY(remote_edit), remote_entry_default);
    gtk_entry_set_editable (GTK_ENTRY(remote_edit), FALSE);
    gtk_entry_set_text(GTK_ENTRY(local_edit), local_source);
    gtk_entry_set_editable (GTK_ENTRY(local_edit), FALSE);
    if (!strcasecmp(user_type, "teacher")){ 
        gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuItemUpload_teac);
        gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuItemDownload_teac);
        gtk_button_set_label (GTK_BUTTON(setup_backup_hw_btn), " ");

    }else{
        gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuItemUpload_stu); 
        gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuItemDownload_stu);        
    }  
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuSeparator);    
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuItemSetup);
    gtk_window_set_title(GTK_WINDOW(setup_window), "設定");

    
    g_signal_connect (G_OBJECT (menuItemUpload_stu), "activate", G_CALLBACK (rupload_data), NULL);
    g_signal_connect (G_OBJECT (menuItemDownload_stu), "activate", G_CALLBACK (rdownload_data), NULL);
    g_signal_connect (G_OBJECT (menuItemSetup), "activate", G_CALLBACK (setup_switch_win), NULL);
    g_signal_connect (G_OBJECT (menuItemUpload_teac), "activate", G_CALLBACK (upload_file), NULL);
    g_signal_connect (G_OBJECT (menuItemDownload_teac), "activate", G_CALLBACK (get_homefile), NULL);
    g_signal_connect(setup_window, "delete_event",G_CALLBACK(delete_event),NULL);
    g_signal_connect (checkbox1, "toggled",G_CALLBACK (usrdir_toggle), NULL);
    g_signal_connect(setup_ok_btn, "clicked",G_CALLBACK(setup_ok_clicked),NULL); 
    g_signal_connect(setup_cancel_btn, "clicked",G_CALLBACK(setup_switch_win),NULL);
    g_signal_connect(setup_backup_hw_btn, "clicked",G_CALLBACK(setup_backup_hw),NULL);     
    g_signal_connect(GTK_STATUS_ICON (tray_icon), "activate", GTK_SIGNAL_FUNC (trayIconPopup), menu);
    gtk_builder_connect_signals(main_builder, NULL);
    
    if (argv[1] != NULL){
        if (!strcasecmp(argv[1],"--padmode")){
            gtk_status_icon_set_visible(tray_icon, FALSE);
            padmode_window = GTK_WIDGET(gtk_builder_get_object(main_builder,"padmode_setup_win"));
            padmode_upload_btn = GTK_WIDGET(gtk_builder_get_object(main_builder,"padmode_setup_ubtn"));
            padmode_download_btn = GTK_WIDGET(gtk_builder_get_object(main_builder,"padmode_setup_dbtn"));
            gtk_widget_set_size_request(padmode_window, 200, 150);
            if (!strcasecmp(user_type, "teacher")){ 
                gtk_button_set_label (GTK_BUTTON(padmode_upload_btn), "上傳作業_teacher");
                gtk_button_set_label (GTK_BUTTON(padmode_download_btn), "下載作業_teacher");
            }else{
                gtk_button_set_label (GTK_BUTTON(padmode_upload_btn), "上傳作業");
                gtk_button_set_label (GTK_BUTTON(padmode_download_btn), "下載作業");
            }
            gtk_widget_show (padmode_window);
        }else{
            gtk_status_icon_set_visible(tray_icon, TRUE);
        }
    }
    
    gtk_widget_show_all (menu);
    gtk_widget_show (UFW_window);   
	gtk_main();
	return 0;
}

