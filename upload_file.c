/* UFW ==> upload file window */

#include <gtk/gtk.h>

GtkWidget *UFW_window;
int UFW_dpy = 1;

extern gchar rselected_file_up[50];
extern void rupload_file_teacher();

static int UFW_win()
{	  
	if (UFW_dpy == 1){
		gtk_widget_show(UFW_window);
		UFW_dpy = 0;
	}else{
		gtk_widget_hide(UFW_window);
		UFW_dpy = 1;
	}
    
    return 0;        
}

gint delete_event_UFW( GtkWidget *widget, GdkEvent  *event, gpointer   data )
{
    g_print ("delete event occurred\n");
    UFW_win();
    return TRUE;
}

void file_changed(GtkFileChooserButton *button, GtkLabel *label) 
{
    gchar *file;
    file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(button));
    gtk_label_set_text(label, file);
    g_printf("selected_file ==>%s\n", file);
    //strncpy(rrselected_file_up, file, 100);
    g_strlcpy(rselected_file_up, file, 100);
}


int upload_file() 
{    
    UFW_dpy = 0;
    gtk_widget_hide(UFW_window);
    
    GtkWidget *UFW_label;
    GtkWidget *UFW_button;
    GtkWidget *UFW_OK_button, *UFW_CANCEL_button;
    GtkFileFilter *filter1, *filter2;
    GtkWidget *UFW_vbox, *UFW_tbox;
    //const gchar *filename = "/home/caterpillar/workspace/caterpillar.gif";
    const gchar *filename = "/root/";

    
    UFW_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(UFW_window), "上傳作業"); 
    //gtk_window_set_default_size(GTK_WINDOW(UFW_window), 250, 100);
    gtk_widget_set_size_request(UFW_window, 300, 150);
    gtk_window_set_position(GTK_WINDOW(UFW_window), GTK_WIN_POS_CENTER);

    UFW_button = gtk_file_chooser_button_new(
                  "選取檔案", GTK_FILE_CHOOSER_ACTION_OPEN);
    gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(UFW_button), filename);
    

    filter1 = gtk_file_filter_new();
    filter2 = gtk_file_filter_new();
    gtk_file_filter_set_name(filter1, "Document Files");
    gtk_file_filter_set_name(filter2, "All Files");
    gtk_file_filter_add_pattern(filter1, "*.txt");
    gtk_file_filter_add_pattern(filter1, "*.doc");
    gtk_file_filter_add_pattern(filter1, "*.ppt");
    gtk_file_filter_add_pattern(filter1, "*.xls");
    gtk_file_filter_add_pattern(filter1, "*.odt");
    gtk_file_filter_add_pattern(filter1, "*.odp");
    gtk_file_filter_add_pattern(filter1, "*.ods");
    gtk_file_filter_add_pattern(filter2, "*");
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(UFW_button), filter1);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(UFW_button), filter2);

    UFW_label = gtk_label_new(filename);

    UFW_vbox = gtk_vbox_new(TRUE, 5);
    gtk_container_add(GTK_CONTAINER(UFW_window), UFW_vbox);
    
    gtk_box_pack_start(GTK_BOX(UFW_vbox), UFW_button, FALSE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(UFW_vbox), UFW_label, FALSE, TRUE, 5);
    
    UFW_tbox = gtk_hbutton_box_new();
    gtk_box_pack_start(GTK_BOX(UFW_vbox), UFW_tbox, FALSE, TRUE, 0);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(UFW_tbox),GTK_BUTTONBOX_SPREAD);
    gtk_button_box_set_child_size(GTK_BUTTON_BOX(UFW_tbox),145,20);
    UFW_OK_button = gtk_button_new_with_label("確定");
    gtk_container_add(GTK_CONTAINER(UFW_tbox), UFW_OK_button);
    UFW_CANCEL_button = gtk_button_new_with_label("取消");
    gtk_container_add(GTK_CONTAINER(UFW_tbox), UFW_CANCEL_button);


    g_signal_connect(GTK_OBJECT(UFW_window), "delete_event",
                     G_CALLBACK(delete_event_UFW), NULL);
    g_signal_connect(GTK_OBJECT(UFW_CANCEL_button), "clicked",
                     G_CALLBACK(delete_event_UFW), NULL);
    g_signal_connect(GTK_OBJECT(UFW_OK_button), "clicked",
                     G_CALLBACK(rupload_file_teacher), NULL);
    g_signal_connect(GTK_OBJECT(UFW_button), "selection_changed",
                     G_CALLBACK(file_changed), UFW_label);

    gtk_widget_show_all(UFW_window);
    return 0;

}
