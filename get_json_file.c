#include <glib.h>
#include <stdio.h>
#include <string.h>
#include "get_json_file.h"


const gchar * strchrn(char *dest, const char *src, char e);
const gchar * get_json_file(char *);
const gchar * get_string(char *src_string, char *item);
char* check_string(char *string);
gchar* g_substr(const gchar *string, gint start, gint end);

gchar *
g_substr (const gchar* string, gint start, gint end)
{
    gsize len = (end - start + 1);
    gchar *output = g_malloc0 (len + 1);
    return g_utf8_strncpy (output, &string[start], len);
}

const gchar * 
get_json_file(char *item)
{
    FILE *fptr;
    char str[max], *stradd;
    int i;
    gchar dest[50];
    fptr=fopen("/usr/share/config/oxpadUser.json","r");
    //fptr=fopen("/tmpp/metadata.json","r");

    if(fptr!=NULL)
    {
        while(!feof(fptr))
        {
            i=fread(str,sizeof(unsigned char),max,fptr);
            if(i<max)
                str[i]='\0';
                stradd = &str[0];
                strncpy(dest, get_string(stradd, item), 50);
                //printf("-----------%s\n", dest);
        }
        fclose(fptr);
    }else
        printf("false!!\n");
    return dest;
}

const gchar * 
get_string(char *src_string, char *item)
{
    char *p;
    char dest_string[20];
    gchar *dest;
        
    int total_len = strlen (src_string);       
    p = g_strstr_len (src_string, max, item);    
    int sub_len = strlen (p);
    int n = total_len - sub_len;

    gchar *result = g_substr(src_string, n+8, n+28);
    dest = strchrn(dest_string, result, '\"');
    
    //printf("++++++++++++%s\n",dest);
    return dest;
}

const gchar * 
strchrn(gchar *dest, const char *src, char e) 
{
    char *n = strchr(src, e);
    int len = strlen(src) - strlen(n);
    
    strncpy(dest, src, len);
    dest[len] = '\0';
    printf("%s\n", dest);
    return dest;
}
