#include <gtk/gtk.h>
#include "rsyncfile.h"


gchar rselected_file[150];
gchar rselected_file_up[150];
gchar *standard_output;
gint finished = 0;
gint check_shomework = 0;
extern gchar *t_file_name[50][50];
extern gchar user_sn[200];
extern gchar user_name[200];
extern const gchar *check_finish;
extern const gchar *t_filename_list;
extern gchar *dir_name[50][20];
extern int t_file_total;
extern int dir_total;
extern int check_getfile_okbutton;
extern void sync_upload_result();
extern void sync_download_result();
extern int get_file_progress (gchar **cmd);

int get_t_filename_list()
{
    FILE *fPtr;
    char strtmp[50];
    
    (void)system(t_filename_list);
    dir_total = 0;
    fPtr = fopen("/tmp/t_filename_list", "r");
    if (!fPtr) {
        printf("檔案開啟失敗...\n");
        exit(1);
    }
     
    while (fgets(strtmp, 50, fPtr) != NULL) {
       	strtmp[strlen(strtmp)-1] = '\0';
        strncpy(t_file_name[t_file_total], strtmp, 50);
        //printf("======%s======\n", t_file_name[t_file_total]);
        t_file_total++;
    }

    fclose(fPtr);
    return 0;
}    

int download_init()
{
    gchar cmd[command_size];
    gchar cmd2[command_size];
    g_snprintf(cmd, command_size,"sshpass -p 123456 rsync -av -h --progress --timeout=30 alan@140.111.80.9:/home/alan/%s/* %s", user_name, "/root/s_homework");
    g_spawn_command_line_async(cmd, NULL);
    g_snprintf(cmd, command_size,"sshpass -p 123456 rsync -av --ignore-existing -h --progress --timeout=30 %s %s", "/tmp/s_homework", "/root/homework/");
    g_spawn_command_line_sync(cmd2,&standard_output,NULL,NULL,NULL);
    printf("standard_out===>>>%s\n",standard_output);
}

int rdownload_file()
{     
    check_shomework = 1;
    gchar cmd[command_size];
    //gchar cmd2[command_size];
    g_snprintf(cmd, command_size,"rm -rf /tmp/s_homework/*", check_finish );
    (void)system(cmd);
    gchar *cmd2[]={"/usr/bin/sshpass", "-p" ,"123456", "/usr/bin/rsync" ,"-av","-h", "--progress", "--timeout=30", "alan@140.111.80.9:/home/alan/homework/" ,"/tmp/s_homework/", 0};
    get_file_progress(&cmd2);
}

void check_s_homework()
{
    gchar cmd[command_size];
    rename_file2();
    g_snprintf(cmd, command_size,"sshpass -p 123456 rsync -av --ignore-existing -h --progress --timeout=30 %s %s %s", "/tmp/s_homework/", "/root/homework/", check_finish );
    g_printf("..............cmd==>%s\n",cmd);
    (void)system(cmd);
    check_shomework = 0;
}


void rupload_file()
{
    gchar cmd[command_size];
    g_snprintf(cmd, command_size,"alan@140.111.80.9:/home/alan/%s/", user_name);
    gchar *cmd2[]={"/usr/bin/sshpass", "-p" ,"123456", "/usr/bin/rsync" ,"-av","-h", "--progress", "/root/homework/" ,cmd, 0};
    get_file_progress(&cmd2);
    //~ if( !system(rupload_use) )
        //~ arguments.check_upload = 1;
}

int rdownload_filelist_teacher()
{
    //gchar cmd[command_size];
    //g_snprintf(cmd, command_size,"sshpass -p 123456 rsync -av alan@140.111.80.9:/home/alan/homework/ %s %s" ,"/tmp/file_list", check_finish);
    //printf("..............cmd==>%s\n",cmd);
    //g_spawn_command_line_async(cmd, NULL);
    //(void)system(cmd);
    get_t_filename_list();
}

int rdownload_file_teacher()
{
    gchar cmd_tmp[command_size];
    gint result;
    int student_sn, i;
    
    if (finished > dir_total-1){
        finished = 0;
        return 0;
    }

    //g_printf("selected ==>%s\n", rselected_file);
    //for (i =0; i<dir_total; i++){
        //printf("dir_name ==> %s\n", dir_name[i]);
        //g_snprintf(cmd, command_size,"sshpass -p 123456 rsync -av -h --progress --timeout=30 alan@140.111.80.9:/home/alan/%s/*%s %s && echo \"OK\"", dir_name[i], rselected_file, "/root/homework/");
        g_snprintf(cmd_tmp, command_size,"alan@140.111.80.9:/home/alan/%s/%s_%s", dir_name[finished], dir_name[finished], rselected_file);
        gchar *cmd[]={"/usr/bin/sshpass", "-p" ,"123456", "/usr/bin/rsync" ,"-av","-h", "--progress", "--timeout=30", cmd_tmp ,"/root/homework/", 0};
        sleep(2);
        check_getfile_okbutton = 1;
        get_file_progress(&cmd);
        
    //}
    
    return 0;
}

int rupload_file_teacher()
{
    //~ gchar cmd[command_size];
    printf("..............uploadfile==>%s\n", rselected_file_up);
    
    //~ g_snprintf(cmd, command_size,"sshpass -p 123456 rsync -av -h --progress %s alan@140.111.80.9:/home/alan/homework/ %s", rselected_file_up, check_finish );
    gchar *cmd[]={"/usr/bin/sshpass", "-p" ,"123456", "/usr/bin/rsync" ,"-av","-h", "--progress", rselected_file_up ,"alan@140.111.80.9:/home/alan/homework/", 0};
    if(!get_file_progress(&cmd))
        arguments.check_upload = 1;
    //~ sync_upload_result();
}

int download_backup_hw()
{
    gchar cmd_tmp[command_size];
    g_snprintf(cmd_tmp, command_size,"alan@140.111.80.9:/home/alan/%s/", user_name);
    gchar *cmd[]={"/usr/bin/sshpass", "-p" ,"123456", "/usr/bin/rsync" ,"-av","-h", "--progress", "alan@140.111.80.9:/home/alan/homework/" ,"/root/homework/", 0};
    get_file_progress(&cmd);
}

int download_backup_hw1()
{
    gchar cmd[command_size];
    g_snprintf(cmd, command_size,"sshpass -p 123456 rsync -av -h --progress alan@140.111.80.9:/home/alan/%s/ %s %s", user_name, "/root/homework/", check_finish );
    printf("..............cmd==>%s\n",cmd);
    //~ g_spawn_command_line_async(cmd, NULL);
    (void)system(cmd);
}
 
 int rename_func2( const char *pathname, const struct stat *statptr, int type)
{
    if (type == 0){
        
        if(!strstr(pathname, user_name))
        {
            printf("%s\n", basename(pathname));
            char file_old_name[200], file_new_name[200];
            sprintf(file_old_name, "/tmp/s_homework/%s", basename(pathname));
            // 有加sn
            //sprintf(file_new_name, "/root/home/%s_%s_%s", user_name, user_sn, basename(pathname));
            // 沒加sn            
            sprintf(file_new_name, "/tmp/s_homework/%s_%s", user_name, basename(pathname));            
            rename(file_old_name, file_new_name);
        }
    }
        return 0;
}
    
int rename_file2()
{
    char path[100];
    int  ret;

    strcpy(path, "/tmp/s_homework/");
    ret = ftw(path, rename_func2, 3);
    if(ret == -1){
        printf("ftw error\n");
        return -1;
    }else{
        printf("操作成功\n");
        return 0;
    }
}
