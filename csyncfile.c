#include "csyncfile.h"

int csync_file(const gchar *_source, const gchar *_target)
{
    printf("This is source %s\n", _source);
    printf("This is target %s\n", _target);
    
    int rc = 0;
    CSYNC *csync;
    char errbuf[256] = {0};

    /* Default values. */
    arguments.exclude_file = NULL;
    arguments.disable_statedb = 0;
    arguments.create_statedb = 0;
    arguments.update = 1;
    arguments.reconcile = 1;
    arguments.propagate = 1;
    arguments.check_upload = 1;
    arguments.with_conflict_copys = false;

    /*
    * Parse our arguments; every option seen by parse_opt will
    * be reflected in arguments.
    */
//    argp_parse (&argp, argc, argv, 0, 0, &arguments);

    if (csync_create(&csync, _source, _target) < 0) {
        fprintf(stderr, "csync_create: failed\n");
        exit(1);
    }

    //~ csync_set_auth_callback(csync, csync_auth);
    //~ if (arguments.disable_statedb) {
        //~ csync_disable_statedb(csync);
    //~ }
  
    if(arguments.with_conflict_copys)
    {
        csync_enable_conflictcopys(csync);
    }

    if (csync_init(csync) < 0) {
        perror("csync_init");
        arguments.check_upload = 0;
        rc = 1;
        goto out;
    }

    if (arguments.exclude_file != NULL) {
        if (csync_add_exclude_list(csync, arguments.exclude_file) < 0) {
            fprintf(stderr, "csync_add_exclude_list - %s: %s\n",
            arguments.exclude_file,
            strerror_r(errno, errbuf, sizeof(errbuf)));
        rc = 1;
        goto out;
        }
    }

    if (arguments.update) {
        if (csync_update(csync) < 0) {
            perror("csync_update");
            rc = 1;
            goto out;
        }
    }

    if (arguments.reconcile) {
        if (csync_reconcile(csync) < 0) {
            perror("csync_reconcile");
            rc = 1;
            goto out;
        }
    }

    if (arguments.propagate) {
        if (csync_propagate(csync) < 0) {
            perror("csync_propagate");
            rc = 1;
            goto out;
        }
    }

    if (arguments.create_statedb) {
        csync_set_status(csync, 0xFFFF);
    }

    out:
        printf("sync_success!\n");
        csync_destroy(csync);
    
    return rc;        
}
