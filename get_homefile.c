/*  FSW ==> file select window  */
/*  pathname ==> path/filename  */

#include <gtk/gtk.h>
 
gchar *t_file_name[50][50];
gchar *dir_name[50][20];
int t_file_total = 0;
int dir_total = 0;
int FSW_dpy = 1;
int check_getfile_okbutton = 0;
GtkBuilder  *FSW_builder;
GtkWidget *FSW_window;

extern gchar rselected_file[50];
const gchar *server_usr_dirname = "sshpass -p 123456 rsync -av alan@140.111.80.9:/home/alan/ | grep '^drw' |grep s0 |cut -d' ' -f12 > /tmp/server_usr_dirname && echo OK";
const gchar *t_filename_list = "sshpass -p 123456 rsync -av alan@140.111.80.9:/home/alan/homework/ | grep '^-rw' |cut -c 44- > /tmp/t_filename_list && echo OK";

enum {
    PIXBUF_COL,
    TEXT_COL
};

gboolean timeout_callback(gpointer data) 
{    
    gdouble value = 0.0;
    GString *text;

    value = gtk_progress_bar_get_fraction(GTK_PROGRESS_BAR(data));

    value += 0.01;
    if(value >= 1.0) {
        return TRUE;
        //value = 0.0;
    }

    if (check_getfile_okbutton)
        gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(data), value);

    text = g_string_new(
             gtk_progress_bar_get_text(GTK_PROGRESS_BAR(data)));
    g_string_sprintf(text, "%d %%", (int) (value * 100));

    gtk_progress_bar_set_text(GTK_PROGRESS_BAR(data), text->str);

    return TRUE;
}

int get_server_usr_dirname()
{
    FILE *fPtr;
    char strtmp[50];
    
    (void)system(server_usr_dirname);
    dir_total = 0;
    fPtr = fopen("/tmp/server_usr_dirname", "r");
    if (!fPtr) {
        printf("檔案開啟失敗...\n");
        exit(1);
    }
     
    while (fgets(strtmp, 50, fPtr) != NULL) {
       	strtmp[strlen(strtmp)-1] = '\0';
        strncpy(dir_name[dir_total], strtmp, 20);
        //printf("======%s======\n", dir_name[dir_total]);
        dir_total++;
    }

    fclose(fPtr);
    return 0;
}

void empty2array()
{
    //~ int i;
    //~ for(i=0; i <=file_total; i++){
        //~ memset(t_file_name[i], 0, 30);
    //~ }
    t_file_total = 0;
}

static int FSW_win()
{	  
	if (FSW_dpy == 1){
		gtk_widget_show(FSW_window);		
		FSW_dpy = 0;
	}else{
		gtk_widget_hide(FSW_window);
		FSW_dpy = 1;
	}
    
    return 0;        
}

gint delete_event_FSW( GtkWidget *widget, GdkEvent  *event, gpointer   data )
{
    g_print ("delete event occurred\n");
    FSW_win();
    return TRUE;
}

int homework_files_download()
{
    //~ int i;
    get_server_usr_dirname();
    rdownload_file_teacher();
    //~ for (i =0; i<dir_total; i++)
        //~ printf("dir_name ==> %s\n", dir_name[i]);
}

GtkTreeModel* createModel() {
    //~ const gchar *t_file_name[] = {"國文作業", "英文作業",
                            //~ "數學作業", "社會作業"};
    GdkPixbuf *pixbuf;
    GtkTreeIter iter;
    GtkListStore *store;
    gint i;

    store = gtk_list_store_new(2, GDK_TYPE_PIXBUF, G_TYPE_STRING);

    for(i = 0; i < t_file_total; i++) {
        //pixbuf = gdk_pixbuf_new_from_file(t_file_name[i], NULL);
        pixbuf = gdk_pixbuf_new_from_file("/usr/share/pixmaps/homework.png", NULL);
        gtk_list_store_append(store, &iter);
        //~ if(i == 1)
            //~ i++;    //跳過[1]因為會出現亂碼....??
        gtk_list_store_set(store, &iter, PIXBUF_COL, pixbuf,
                           TEXT_COL, t_file_name[i], -1);                                   
        g_object_unref(pixbuf);        
    }
    return GTK_TREE_MODEL(store);
}

gboolean selection_changed(GtkTreeSelection *selection, GtkLabel *label) {
    
    GtkTreeView *treeView;
    GtkTreeModel *model;
    GtkTreeIter iter;
    gchar *active;
    
    treeView = gtk_tree_selection_get_tree_view(selection);
    model = gtk_tree_view_get_model(treeView);
    gtk_tree_selection_get_selected(selection, &model, &iter);
    gtk_tree_model_get(model, &iter, 1, &active, -1);
    gtk_label_set_text(label, active);
    strncpy(rselected_file, active, 50);
	//g_printf("selected ==>%s\n", rselected_file);
}

int get_homefile(){
    
    FSW_dpy = 0;
    gtk_widget_hide(FSW_window);
    empty2array();
    rdownload_filelist_teacher();
    //~ sleep(2);   //延遲時間讓檔案下載完成後可以執行files_list
    //~ files_list();
    
    GtkWidget *treeView;
    GtkCellRenderer *renderer;
    GtkTreeViewColumn *column;
    GtkWidget *label;
    GtkWidget *vbox;
    GtkWidget *hbox, *tbox;
    GtkTreeSelection *selection;
    GtkWidget *Ok_Button;
    GtkWidget *Cancel_Button;
    GtkWidget *FSW_scrollw;
    
    GtkWidget *progressBar;
    gint timer; 
    
    FSW_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    FSW_scrollw = gtk_scrolled_window_new(NULL, NULL);
    gtk_window_set_title(GTK_WINDOW(FSW_window), "HOMEWORK");
    gtk_window_set_default_size(GTK_WINDOW(FSW_window), 250, 200);
    gtk_window_set_position(GTK_WINDOW(FSW_window), GTK_WIN_POS_CENTER);
    gtk_widget_set_size_request(FSW_scrollw, 100, 200);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(FSW_scrollw), GTK_POLICY_NEVER,GTK_POLICY_AUTOMATIC);   

    treeView = gtk_tree_view_new_with_model(createModel());

    renderer = gtk_cell_renderer_pixbuf_new();
    column = gtk_tree_view_column_new_with_attributes("Icon", renderer, 
                     "pixbuf", PIXBUF_COL, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW (treeView), column);

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes(
                     "Filename", renderer, "text", TEXT_COL, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW (treeView), column);


    label = gtk_label_new("test");
    vbox = gtk_vbox_new(FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), FSW_scrollw, TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
    gtk_container_add (GTK_CONTAINER (FSW_scrollw), treeView);
    gtk_container_add(GTK_CONTAINER(FSW_window), vbox);    
    hbox = gtk_hbox_new(TRUE,0);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);
    tbox = gtk_hbutton_box_new();
    gtk_box_pack_start(GTK_BOX(vbox), tbox, FALSE, TRUE, 0);
    gtk_button_box_set_layout(GTK_BUTTON_BOX(tbox),GTK_BUTTONBOX_SPREAD);
    gtk_button_box_set_child_size(GTK_BUTTON_BOX(tbox),150,20);
    Ok_Button = gtk_button_new_with_label("確定");
    gtk_container_add(GTK_CONTAINER(tbox), Ok_Button);
    Cancel_Button = gtk_button_new_with_label("取消");
    gtk_container_add(GTK_CONTAINER(tbox), Cancel_Button);
    
    progressBar = gtk_progress_bar_new();
    timer = gtk_timeout_add(100, timeout_callback, progressBar);
    //gtk_container_add(GTK_CONTAINER(vbox), progressBar);
    
    
    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeView));
    g_signal_connect(G_OBJECT(selection), "changed",G_CALLBACK(selection_changed), label);
    g_signal_connect(G_OBJECT(FSW_window), "delete_event",G_CALLBACK(delete_event_FSW), NULL);
    g_signal_connect(G_OBJECT(Ok_Button), "clicked",G_CALLBACK(homework_files_download), NULL);                     
    g_signal_connect(G_OBJECT(Cancel_Button), "clicked",G_CALLBACK(delete_event_FSW), NULL);
                     
    gtk_widget_show_all(FSW_window);
    
    return 0;
}
