#include <argp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include "csync.h"

const gchar *_source;
const gchar *_target;

struct argument_s {
  char *args[2]; /* SOURCE and DESTINATION */
  char *exclude_file;
  int disable_statedb;
  int create_statedb;
  int update;
  int reconcile;
  int propagate;
  int check_upload;
  bool with_conflict_copys;
};

struct argument_s arguments;

int csync_file(const gchar *_source, const gchar *_target);

