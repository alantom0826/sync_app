#include <gtk/gtk.h>

extern void setup_win(GtkWidget *setup_window, gpointer data);


GtkBuilder *setup_builder;
GtkWidget *setup_window;

void setup_win(GtkWidget *setup_window, gpointer data)
{
	setup_builder = gtk_builder_new();
	gtk_builder_add_from_file(setup_builder,"setup.ui",NULL);	
	
	setup_window=GTK_WIDGET(gtk_builder_get_object(setup_builder,"dialog1"));
	
	//	button2=GTK_WIDGET(gtk_builder_get_object(builder,"button2"));
	
	//	g_signal_connect(button2,"clicked",G_CALLBACK(window_close),NULL);
	gtk_widget_show(setup_window);
}
