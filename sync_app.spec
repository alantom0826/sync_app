Name:		sync_app
Version:	0.1
Release:	5%{?dist}
Summary:	synchronize and upload file by rsync.
License:	GPL	
Source0:	%{name}-%{version}.tar.gz

Requires:	rsync, sshpass
BuildRequires:  glib2, gtk2-devel

%description
upload file to server and synchronize file to client.

%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%post
key=1
cat /root/.ssh/known_hosts |grep "140.111.80.9" && key=0
if [ $key == 1 ];then
cat /root/.ssh/sshkey >> /root/.ssh/known_hosts
fi

%files
%{_datadir}/%{name}/%{name}
%{_datadir}/%{name}/main.glade
%{_bindir}/mysync
%{_bindir}/mysync_p
%{_datadir}/pixmaps/sync_tray.png
%{_datadir}/pixmaps/menu_download.png
%{_datadir}/pixmaps/menu_upload.png
%{_datadir}/pixmaps/menu_setup.png
%{_datadir}/pixmaps/homework.png
%{_datadir}/applications/%{name}.desktop
/root/.ssh/sshkey

%changelog
* Mon Jan 14 2013 AlanTom <alan.tom@ossii.com.tw> ossii -0.1-5
- Add padmode argument for sync_app padmode.
- Add sync_app.desktop file.

* Wed Jan 09 2013 AlanTom <alan.tom@ossii.com.tw> ossii -0.1-4
- Add Requires rsync and sshpass.

* Thu Jan 08 2013 AlanTom <alan.tom@ossii.com.tw> ossii -0.1-3
- Modify student download homework overwrite file.
- Add teacher upload file messages.
- Add sshkey file for ssh connect.

* Wed Jan 02 2013 AlanTom <alan.tom@ossii.com.tw> ossii -0.1-2
- Modify teacher list file model.
- Modify student download homework model.
- Add  student mode setup->backup homework.

* Mon Nov 05 2012 AlanTom <alan.tom@ossii.com.tw> ossii -0.1-1
- first Initial the package.


